import { NgModule } from '@angular/core';
import { ItemToggleComponent } from './item-toggle/item-toggle.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TitleDirective } from './directive/title.directive';
import { ContentDirective } from './directive/content.directive';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
    ],
    declarations: [
        ItemToggleComponent,
        TitleDirective,
        ContentDirective,
    ],
    exports: [
        ItemToggleComponent,
        TitleDirective,
        ContentDirective,
    ],
})
export class ComponentsModule {
}
