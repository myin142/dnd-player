import { Component, ContentChild, Input, TemplateRef } from '@angular/core';
import { TitleDirective } from '../directive/title.directive';
import { ContentDirective } from '../directive/content.directive';

@Component({
    selector: 'app-item-toggle',
    templateUrl: './item-toggle.component.html',
})
export class ItemToggleComponent {

    @ContentChild(TitleDirective, {read: TemplateRef, static: true})
    titleTemplate: TemplateRef<any>;

    @ContentChild(ContentDirective, {read: TemplateRef, static: false})
    contentTemplate: TemplateRef<any>;

    @Input() collapsed = false;
    @Input() fullClick = false;

    get lines(): string {
        return this.collapsed ? 'full' : 'none';
    }

    get dropIcon(): string {
        return this.collapsed ? 'arrow-dropdown' : 'arrow-dropleft';
    }

}
