import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuCloseComponent } from './menu-close/menu-close.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        MenuCloseComponent,
    ],
    exports: [
        MenuCloseComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
})
export class MenuModule {
}
