import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-menu-close',
    templateUrl: './menu-close.component.html',
})
export class MenuCloseComponent implements OnInit {

    constructor(private modal: ModalController) {
    }

    ngOnInit() {
    }

}
