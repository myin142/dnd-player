import { Component, OnInit } from '@angular/core';
import { ActivePlayerService } from '../services/active-player.service';
import { Observable } from 'rxjs';
import { DndPlayer } from 'dnd-shared';

@Component({
    selector: 'app-active-player',
    templateUrl: './active-player.page.html',
    styleUrls: ['./active-player.page.scss'],
})
export class ActivePlayerPage implements OnInit {

    activePlayer: Observable<DndPlayer>;

    constructor(private activePlayerService: ActivePlayerService) {
    }

    ngOnInit() {
        this.activePlayer = this.activePlayerService.get();
    }

    saveActivePlayer() {
        this.activePlayerService.saveActivePlayer();
    }

}
