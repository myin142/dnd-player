import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ActivePlayerPage } from './active-player.page';

const routes: Routes = [
    {
        path: '',
        component: ActivePlayerPage,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        ActivePlayerPage,
    ],
})
export class ActivePlayerPageModule {
}
