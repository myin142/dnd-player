import { Injectable } from '@angular/core';
import { ZeroconfService } from '@ionic-native/zeroconf/ngx';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DndClass, DndSubclass, AbilityScore } from 'dnd-shared';

@Injectable({
    providedIn: 'root',
})
export class DndMasterService {

    private masterService: ZeroconfService;

    constructor(private http: HttpClient) {
    }

    setService(service: ZeroconfService): void {
        console.log(`Set Service`, service);
        this.masterService = service;
    }

    hasSelectedService(): boolean {
        return this.masterService != null;
    }

    getAbilityScores(): Observable<AbilityScore[]> {
        return this.getData(`abilityScores`);
    }

    getClasses(): Observable<DndClass[]> {
        return this.getData(`classes`);
    }

    getSubclasses(className: string): Observable<DndSubclass[]> {
        return this.getData(`subclasses`, { class: className });
    }

    private getData<T>(url: string, query: object = {}): Observable<T> {
        if (!this.hasSelectedService()) {
            return throwError(`No DND master service selected`);
        }

        const queryStr = this.createQueryParam(query);
        return this.http.get<T>(`${this.url}/${url}?${queryStr}`);
    }

    private createQueryParam(params: object): string {
        return Object.keys(params).map(key => `${key}=${params[key].trim()}`).join('&');
    }

    private get url(): string {
        const ip = this.masterService.ipv4Addresses[0] || `[${this.masterService.ipv6Addresses[0]}]`;
        return `http://${ip}:${this.masterService.port}`;
    }

}
