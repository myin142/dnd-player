import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage/';
import { map, tap } from 'rxjs/operators';
import { Utils } from '../shared/utils';
import { DndPlayer } from 'dnd-shared';

@Injectable({
    providedIn: 'root',
})
export class DndPlayerService {

    private PLAYER_KEY = 'dnd-player-key';
    private players = new BehaviorSubject<DndPlayer[]>([]);

    constructor(private storage: Storage) {
        this.storage.get(this.PLAYER_KEY).then(players => {
            if (players != null && players.length > 0) {
                this.players.next(players);
            }

            this.players.subscribe(newPlayers => {
                this.storage.set(this.PLAYER_KEY, newPlayers);
            });
        });
    }

    getPlayers(): Observable<DndPlayer[]> {
        return this.players.pipe(
            map(players => this.sortPlayers(players)),
            tap(players => console.log(`Loading Players`, players)),
        );
    }

    private sortPlayers(players: DndPlayer[]): DndPlayer[] {
        console.log('Sort', players);
        return players.sort((p1, p2) => p1.name.localeCompare(p2.name));
    }

    updatePlayer(player: DndPlayer): void {
        this.players.next([
            ...this.getPlayersWithout(player),
            player,
        ]);
    }

    createPlayer(player: DndPlayer): void {
        const newPlayer = this.newPlayerWithUUID(player);
        this.players.next([
            ...this.players.value,
            newPlayer,
        ]);
    }

    private newPlayerWithUUID(player: DndPlayer): DndPlayer {
        const id = Utils.generateUUID(uuid => this.players.value.find(p => p.uuid === uuid) != null);
        return {
            ...player,
            uuid: id,
        };
    }

    reset(): void {
        this.players.next([]);
    }

    deletePlayer(player: DndPlayer): void {
        const filteredPlayers = this.getPlayersWithout(player);
        this.players.next([
            ...filteredPlayers,
        ]);
    }

    private getPlayersWithout({uuid}: DndPlayer): DndPlayer[] {
        return this.players.value.filter(p => p.uuid !== uuid);
    }

}
