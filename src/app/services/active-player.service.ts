import { Injectable } from '@angular/core';
import { DndPlayer } from 'dnd-shared';
import { Storage } from '@ionic/storage';
import { StorageService } from '../shared/storage.service';
import { DndPlayerService } from './dnd-player.service';
import { first } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class ActivePlayerService extends StorageService<DndPlayer> {

    private static ACTIVE_PLAYER_KEY = 'dnd-active-player-key';

    constructor(private storage: Storage,
                private dndPlayer: DndPlayerService) {
        super(storage, ActivePlayerService.ACTIVE_PLAYER_KEY);
    }

    saveActivePlayer(): void {
        this.get().pipe(first()).subscribe(player => {
            console.log('Save Active Player', player);
            this.dndPlayer.updatePlayer(player);
            this.set(null);
        });
    }

}
