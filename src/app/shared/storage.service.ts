import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, skip, switchMap } from 'rxjs/operators';

export abstract class StorageService<T> {

    private value = new BehaviorSubject<T>(null);
    private initialized = new BehaviorSubject<boolean>(false);

    constructor(storage: Storage,
                storageKey: string) {
        storage.get(storageKey).then(value => {
            console.log(`Load ${storageKey}`, value);
            this.value.next(value);
            this.initialized.next(true);

            // Skip last value that we just received
            this.value.pipe(skip(1)).subscribe(changedValue => {
                console.log(`Save ${storageKey}`, changedValue);
                storage.set(storageKey, changedValue);
            });
        });
    }

    set(newValue: T): void {
        this.value.next(newValue);
    }

    get(): Observable<T> {
        return this.initialized.pipe(
            filter(x => x),
            switchMap(() => this.value),
        );
    }

}
