import * as uuid from 'uuid/v4';

export class Utils {

    public static generateUUID(existingUUID: (id: string) => boolean = () => false): string {
        let id = null;
        while (id == null || existingUUID(uuid)) {
            id = uuid();
        }

        return id;
    }

}
