import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DndMasterService } from '../../services/dnd-master.service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { AbilityScore } from 'dnd-shared';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-set-ability-scores',
    templateUrl: './set-ability-scores.component.html',
})
export class SetAbilityScoresComponent implements OnInit {

    abilityScores: AbilityScore[];
    abilityForm: FormArray;
    @Output() formChange = new EventEmitter<AbilityScore[]>();

    constructor(private builder: FormBuilder,
                private dndMaster: DndMasterService) {
    }

    ngOnInit() {
        this.dndMaster.getAbilityScores().subscribe(abilityScores => {
            this.abilityScores = abilityScores;
            this.abilityForm = this.createAbilityScoreForms(abilityScores);

            this.abilityForm.valueChanges.pipe(debounceTime(100)).subscribe(values => {
                this.formChange.emit(values);
            });
        });
    }

    private createAbilityScoreForms(abilityScores: AbilityScore[]): FormArray {
        return this.builder.array(abilityScores.map(({name, skills}) => this.builder.group({
            name,
            value: 10,
            proficient: false,
            skills: this.builder.array(skills.map(skill => this.builder.group({
                name: skill,
                proficient: false,
            }))),
        })));
    }

    getFormGroupAt(index: number): FormGroup {
        return (this.abilityForm as FormArray).at(index) as FormGroup;
    }

    getSkillControl(parentIdx: number): FormArray {
        return this.getFormGroupAt(parentIdx).controls.skills as FormArray;
    }

}
