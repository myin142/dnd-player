import { Component, OnInit } from '@angular/core';
import { DndPlayerService } from '../services/dnd-player.service';
import { AlertController, ModalController } from '@ionic/angular';
import { NewPlayerComponent } from './new-player/new-player.component';
import { Observable } from 'rxjs';
import { DndPlayer } from 'dnd-shared';
import { SearchServiceComponent } from '../search-service/search-service.component';
import { DndMasterService } from '../services/dnd-master.service';
import { ActivePlayerService } from '../services/active-player.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-manage-player',
    templateUrl: './manage-player.page.html',
})
export class ManagePlayerPage implements OnInit {

    players: Observable<DndPlayer[]>;

    constructor(private dndPlayer: DndPlayerService,
                private dndMaster: DndMasterService,
                private activePlayer: ActivePlayerService,
                private router: Router,
                private modal: ModalController,
                private alert: AlertController) {
    }

    ngOnInit() {
        this.players = this.dndPlayer.getPlayers();
    }

    setActivePlayer(player: DndPlayer): void {
        this.activePlayer.set(player);
        this.router.navigateByUrl('/active-player');
    }

    resetPlayers(): void {
        this.dndPlayer.reset();
    }

    async checkDndMasterService(): Promise<void> {
        if (this.dndMaster.hasSelectedService()) {
            return await this.openNewPlayer();
        }

        const alert = await this.alert.create({
            header: 'No DND Master Service',
            message: 'Without a DND Master Service. You cannot create a new player.',
            buttons: [
                {text: 'Search Service', handler: () => this.openSearchService()},
                {text: 'Cancel', role: 'cancel'},
            ],
        });

        return await alert.present();
    }

    async openSearchService(): Promise<void> {
        const modal = await this.modal.create({
            component: SearchServiceComponent,
        });

        await modal.present();
    }

    async openNewPlayer(): Promise<void> {
        const modal = await this.modal.create({
            component: NewPlayerComponent,
        });

        await modal.present();

        const {data} = await modal.onDidDismiss();
        if (data) {
            this.dndPlayer.createPlayer(data);
        }
    }

    deletePlayer(player: DndPlayer): void {
        this.dndPlayer.deletePlayer(player);
    }

}
