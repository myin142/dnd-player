import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-new-player',
    templateUrl: './new-player.component.html',
})
export class NewPlayerComponent implements OnInit {

    formGroup: FormGroup;

    constructor(private builder: FormBuilder,
                private modal: ModalController) {
    }

    ngOnInit() {
        this.formGroup = this.builder.group({
            name: [null, Validators.required],
            abilityScores: [null, Validators.required],
        });
    }

    createNewPlayer(): void {
        if (this.formGroup.valid) {
            this.modal.dismiss({
                ...this.formGroup.value,
            });
        } else {
            this.formGroup.markAllAsTouched();
        }
    }

    updateFormValues(formName: string, value: any): void {
        this.formGroup.controls[formName].setValue(value);
    }

}
