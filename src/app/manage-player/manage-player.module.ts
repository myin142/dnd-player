import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewPlayerComponent } from './new-player/new-player.component';
import { MenuModule } from '../menu/menu.module';
import { ManagePlayerPage } from './manage-player.page';
import { ComponentsModule } from '../components/components.module';
import { SearchServiceModule } from '../search-service/search-service.module';
import { SetAbilityScoresComponent } from './set-ability-scores/set-ability-scores.component';

const routes: Routes = [
    {
        path: '',
        component: ManagePlayerPage,
    },
];

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        MenuModule,
        ComponentsModule,
        SearchServiceModule,
    ],
    declarations: [
        ManagePlayerPage,
        NewPlayerComponent,
        SetAbilityScoresComponent,
    ],
    entryComponents: [
        NewPlayerComponent,
    ],
})
export class ManagePlayerModule {
}
