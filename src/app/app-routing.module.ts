import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {path: 'manage-player', loadChildren: () => import('./manage-player/manage-player.module').then(m => m.ManagePlayerModule)},
    {path: 'active-player', loadChildren: () => import('./active-player/active-player.module').then(m => m.ActivePlayerPageModule)},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
