import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DiscoveryService } from './discovery.service';
import { Observable } from 'rxjs';
import { ZeroconfService } from '@ionic-native/zeroconf';
import { ModalController } from '@ionic/angular';
import { DndMasterService } from '../services/dnd-master.service';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-search-service',
    templateUrl: './search-service.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush, // Default does not work
})
export class SearchServiceComponent implements OnInit {

    private services: Observable<ZeroconfService[]>;

    constructor(private discoveryService: DiscoveryService,
                private dndMaster: DndMasterService,
                private changeRef: ChangeDetectorRef,
                private modal: ModalController) {
    }

    ngOnInit() {
        this.services = this.discoveryService.searchServices().pipe(
            tap(s => {
                console.log(s);
            }),
            tap(() => this.changeRef.detectChanges()),
        );
    }

    setService(service: ZeroconfService): void {
        this.dndMaster.setService(service);
        this.modal.dismiss();
    }

}
