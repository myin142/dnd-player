import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Zeroconf } from '@ionic-native/zeroconf/ngx';
import { DiscoveryService } from './discovery.service';
import { SearchServiceComponent } from './search-service.component';
import { MenuModule } from '../menu/menu.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MenuModule,
    ],
    declarations: [SearchServiceComponent],
    entryComponents: [SearchServiceComponent],
    providers: [
        Zeroconf,
        DiscoveryService,
    ],
})
export class SearchServiceModule {
}
