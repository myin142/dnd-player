import { Injectable } from '@angular/core';
import { Zeroconf, ZeroconfService } from '@ionic-native/zeroconf/ngx';
import { Observable } from 'rxjs';

@Injectable()
export class DiscoveryService {

    serverType = '_dnd-service._tcp.';
    serverDomain = 'local.';
    serverHost = 'DND-Master';

    constructor(private zeroconf: Zeroconf) {
    }

    searchServices(): Observable<ZeroconfService[]> {
        return new Observable<ZeroconfService[]>(observer => {
            const services: ZeroconfService[] = [];

            this.zeroconf.watch(this.serverType, this.serverDomain).subscribe(result => {
                const serviceIndex = services.findIndex(x => x.name === result.service.name);

                if (result.action === 'removed' && serviceIndex !== -1) {
                    console.log('Remove Service', result);
                    services.splice(serviceIndex, 1);
                    observer.next(services);

                    // Use resolved since it contains ip addresses
                } else if (result.action === 'resolved' && serviceIndex === -1) {
                    console.log('Add Service', result);
                    services.push(result.service);
                    observer.next(services);
                }
            });

            return async () => await this.zeroconf.unwatch(this.serverType, this.serverDomain);
        });
    }

}
